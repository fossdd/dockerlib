import os
import sys

# Print message and exit with exit_code
def error(message, exit_code):
    print(message)
    sys.exit(exit_code)

# Return environment variable, if not exist return `default`
def env0r(env, default):
    try:
        return os.environ[env]
    except KeyError:
        return default

# Return environment variable, print error and exit if not given
def env_required(env):
    try:
        return os.environ[env]
    except KeyError:
        error("Please assign environment variable: " + env, 1)