import toml

# Loads a single image
def image(dir, name):

    # Parse the file into the "single image" dict
    try:
        # Try the version user entered
        f_dict = toml.load(f"{dir}/{name}.toml", _dict=dict)
    except FileNotFoundError:
        try:
            # Try without `:latest`-Tag
            f_dict = toml.load(f"{dir}/{name}.toml".replace(":latest", ""), _dict=dict)
        except FileNotFoundError:
            try:
                # Try with `:latest`-Tag
                f_dict = toml.load(f"{dir}/{name}:latest.toml", _dict=dict)
            except FileNotFoundError:
                import helper
                helper.error(f"Can't find `{name}` in image configurations", 1)
                
    return f_dict