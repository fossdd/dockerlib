# dockerlib

> Build official Docker Images yourself without Raid Limit instead using Docker Hub

## Installation

Build it using the makefile:

```
make
```

Now, to install, you have just to run: `sudo make install`

## Usage

To build, for example Alpine, run:

`sudo dockerlib build alpine`